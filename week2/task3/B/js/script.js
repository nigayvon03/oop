const countMaxNumbers = () => {
    const numbersInput = document.getElementById('numbers');
    const numbers = numbersInput.value.split(',').map(Number);

    const maxNumber = getMaxNumber(numbers);
    const count = countNumbers(numbers, maxNumber);

    const resultText = "Количество чисел, равное максимальному, равно: " + count;

    displayResult(resultText);
}

const getMaxNumber = numbers => Math.max(...numbers);

const countNumbers = (numbers, target) => {
    let count = 0;

    for (let i = 0; i < numbers.length; i++) {
        if (numbers[i] === target) {
            count++;
        }
    }

    return count;
}

const displayResult = resultText => {
    const resultElement = document.getElementById('result');
    resultElement.innerText = resultText;
}

document.getElementById('numbers').addEventListener('input', countMaxNumbers);