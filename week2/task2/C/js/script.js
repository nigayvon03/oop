//Функция для рисования фигуры
let canvas = document.getElementById("myCanvas");
let context = canvas.getContext("2d");

const drawFig = (x, y) => {
  context.fillStyle = 'Red';
  context.fillRect(x, y, 10, 10);
};

const drawPyramid = (n) => {
  context.clearRect(0, 0, canvas.width, canvas.height);

  for (let x = 10; x < n * 10; x += 10) {
    for (let y = x; y < n * 10; y += 10) {
      drawFig(285 + 15 + x * 1.5, y * 1.5);
      drawFig(310 - 15 - x * 1.5, y * 1.5);
    };
  };
};

const drawPyramidFunction = () => {
  let n = parseFloat(document.getElementById('iv').value);
  drawPyramid(n);
};