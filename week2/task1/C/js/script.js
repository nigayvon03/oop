const checkBadminton = () => {
    const weekday = document.getElementById('weekday').value;
    const temperature = document.getElementById('temperature').value;
    const precipitation = document.getElementById('precipitation').value;
    const wind = document.getElementById('wind').value;
    const humidity = document.getElementById('humidity').value;

    if (weekday === "sunday" && 
        ( temperature === 'warm' || temperature === 'hot') && 
        (precipitation === 'sunny' || precipitation === 'cloudy' )&& 
        wind === 'no' && 
        (humidity === 'low'|| humidity === 'high')
        ) {
        document.getElementById('result').textContent = 'Да';
    }
    else {
        document.getElementById('result').textContent = 'Нет';
    }
}