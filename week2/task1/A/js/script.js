const declination = age => {
  let lastDigit = age % 10;
  let lastTwoDigits = age % 100;

  if (lastTwoDigits >= 11 && lastTwoDigits <= 19) {
    return 'лет';
  } else if (lastDigit === 1) {
    return 'год';
  } else if (lastDigit >= 2 && lastDigit <= 4) {
    return 'года';
  } else {
    return 'лет';
  }
}
const showAge = () => {
  let age = parseInt(document.getElementById('ageInput').value);
  let result = age + ' ' + declination(age);
  document.getElementById('result').innerText = 'Поздравляю, что Вам, ' + result;
}