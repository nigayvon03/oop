const calculateDaysInMonth = month => {
    if (month == 2) {
        let year = new Date().getFullYear();
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
            return 29; // Високосный год
        } else {
            return 28; // Невисокосный год
        }
    }
    else if (month == 4 || month == 6 || month == 9 || month == 11) {
        return 30;
    }
    else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        return 31
    }
    else {
        return "Не верный номер месяца";
    }
}

document.getElementById('calculateBtn').addEventListener('click', () => {
    var monthInput = document.getElementById('monthInput').value;
    var result = calculateDaysInMonth(monthInput);
    document.getElementById('result').textContent = 'Количество дней в месяце: ' + result;
});