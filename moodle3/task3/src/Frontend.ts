import * as readlineSync from "readline-sync";
import { CartSeatsAmount } from "./models/TicketRepository";
import * as fs from "fs";
import { Train } from "./models/Train";
import { Timetable } from "./models/Timetable";
import { Station } from "./models/Station";
import Backend from "./Backend";

export class Frontend {
  private users: { [key: string]: { phone: string; email: string } } = {};
  private backend: Backend;
  constructor() {
    this.backend = new Backend();
  }

  public showMenu(): number {
    console.clear();
    console.log("Welcome to the Application!");
    console.log("Please select your role:");
    console.log("1. User");
    console.log("2. Administrator");
    console.log("3. Exit");

    let result = readlineSync.question("Enter menu item number ");

    switch (result) {
      case "1":
        console.clear();
        console.log("----User authorization----");
        this.userAuthorization();
        break;
      case "2":
        console.clear();
        console.log("----Administrator authorization----");
        this.adminAuthorization();
        
        break;
      case "3":
        return 42;
      default:
        console.log("Invalid menu item number");
        break;
    }
    return 0;
  }

  public userAuthorization(): void {
    console.clear();
    console.log("---- User authorization ----");
    const firstName = readlineSync.question("Enter first name: ");
    const lastName = readlineSync.question("Enter last name: ");


    if (this.backend.checkUser(firstName, lastName)) {
      console.clear();
      console.log('You are already logged in');
      const qwe = readlineSync.question('Press enter to continue');
      this.showMainOptions();
    } else {
      console.log('User not found.');
      const yn = readlineSync.question('Create new user? [y/n]');
      if (yn === 'y') {
        const email = readlineSync.question('!!!Enter email: ');
        const password = readlineSync.question('Enter password: ');
        this.backend.addUser(firstName, lastName, email, password);
        const qwe = readlineSync.question('press enter to continue');
        console.clear();
        console.log('User added');
        this.showMainOptions();
      } else {
        this.showMenu();
      }
    }
  }

  private adminAuthorization(): void {
    console.clear();
    console.log('---- Administrator authorization ----');
    const firstName = readlineSync.question('Enter first name: ');
    const lastName = readlineSync.question('Enter last name: ');
    const AdminID = readlineSync.question('Enter AdminID: ');
  
    if (this.backend.checkAdmin(firstName, lastName, AdminID)) {
      console.log('Administrator login successful');
      const qwe = readlineSync.question('press enter to continue');
      this.showAdminOptions();
    } else {
      console.log('Administrator not found.');
      const qwe = readlineSync.question('Press enter to go to the main menu');
    }
  }
  

  private showAdminOptions(): void {
    console.clear();
    console.log('---- Administrator Options ----');
    console.log('1. Edit Trains');
    console.log('2. Edit Stations');
    console.log('3. Back to Main Menu');

    let result = readlineSync.question('Enter menu item number ');

    switch (result) {
      case '1':
        console.clear();
        console.log('---- Edit Trains ----');
        this.editTrain();
        // this.showAdminOptions();
        break;
      case '2':
        console.clear();
        console.log('---- Edit Stations ----');
        this.editStation();
        // this.showAdminOptions();
        break;
      case '3':
        console.clear();
        this.showMenu();
        break;
      default:
        console.log('Invalid menu item number');
        this.showAdminOptions();
        break;
    }
  }
  public editStation(): void {
    console.clear();
    console.log('--- Edit Stations ---');
    console.log('1. Add Station');
    console.log('2. Delete Station');
    console.log('3. View Stations');
    console.log('4. Back to Administrator Options');
    let result = readlineSync.question('Enter menu item number: ');
    switch (result) {
      case '1':
        //add station
        console.clear();
        console.log('--- Add Station ---');
        let stationName = readlineSync.question('Enter station name: ');
        this.backend.addStation(stationName);
        const qwe = readlineSync.question('Press enter to go to the Edit Stations');
        this.editStation();
        break;
      case '2':
        //delete station
        console.clear();
        console.log('--- Delete Station ---');
        let stationId = readlineSync.question('Enter station id: ');
        this.backend.deleteStation(stationId);
        console.log('Station deleted');
        const qwe2 = readlineSync.question('Press enter to go to the Administrator Options');
        this.showAdminOptions();
        break;
      case '3':
        //view stations
        console.clear();
        console.log('--- View Stations ---');
        console.log(this.backend.getAllStation());
        const qwe3 = readlineSync.question('Press enter to go to the Administrator Options');
        this.showAdminOptions();
        break;
      case '4':
        // Back to Administrator Options
        break;
      default:
        console.log('Invalid menu item number');
        break;
    }
  }

  public editTrain(): void {
    console.clear();
    console.log('---- Edit Trains ----');
    console.log('1. Add Train');
    console.log('2. Delete Train');
    console.log('3. View Trains');
    console.log('4. Back to Administrator Options');
    let result = readlineSync.question('Enter menu item number: ');
    switch (result) {
      case '1':
        //add train
        console.clear();
        console.log('---- Add Train ----');
        let startStationName = readlineSync.question('Enter start station name: ');
        let endStationName = readlineSync.question('Enter end station name: ');
        this.backend.addTrain(startStationName, endStationName);

        console.log('Train added');
        break;
      case '2':
        //delete train
        console.clear();
        console.log('--- Delete Train ---');
        let trainId = readlineSync.question('Enter train id: ');
        this.backend.deleteTrain(trainId);
        console.log('Train deleted');
        break;
      case '3':
        //view trains
        console.clear();
        console.log('--- View Trains ---');
    }
  }

  public showMainOptions(): void {
    console.clear();
    console.log("---- User Options ----");
    console.log("1. Seat Reservation");
    console.log("2. Delete account");
    console.log("3. Go to the main menu");

    let result = readlineSync.question("Enter menu item number ");

    switch (result) {
      case "1":
        console.clear();
        //this.seatReservation();
        break;
        case "2":
          console.clear();
          console.log("--- Delete User ---");
          let password: string = readlineSync.question("Enter your password: ");
          this.backend.deleteUser(password);
          break;
        
      case "3":
        console.clear();
        break;
      default:
        console.log("Invalid menu item number");
        break;
    }
  }
  // public seatReservation(): void {
  //   console.log('--- Seat Reservation ---');
  //   console.log('1. Type cart');
  //   console.log('2. Type reserved seat');
  //   console.log('3  Number seat')
  // }
  // public seatReservation(): void {
  //     console.clear();
  //     let seatId = parseInt(readlineSync.question('Which place will we choose?'));
  //     if (seatId > this.cart.getSeatsAmount() || seatId <= 0) {
  //       console.log("AAAAAAAAAAAAAAAAAAAAAAA!");
  //     } else {
  //       this.cart.getSeats()[seatId - 1]?.setStatus(true);
  //       console.log(this.cart);
  //     }
  //     let res = readlineSync.question('Press Enter to exit...');
  //     console.clear();
  // }
}
