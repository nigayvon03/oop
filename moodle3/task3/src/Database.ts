import * as fs from 'fs';
import { Station } from './models/Station';
import { User } from './models/User';
import { Admin } from './models/Admin';
import { Train } from './models/Train';

export class Database {
    //---Users---
    public addUser(user: User): void {
      const existingUsers = this.getAllUsers();
      const newId = existingUsers.length + 1;
      user.setId(newId);
      existingUsers.push(user);
      this.saveUsersToFile(existingUsers);
    }
    public getAllUsers(): User[] {
      try {
        const data = fs.readFileSync('Database/user.json', 'utf-8');
        if (!data) {
          return [];
        }
        const userData = JSON.parse(data);
        return userData.map((user: any) => new User(user.id, user.firstName, user.lastName, user.email, user.password));
      } catch (err) {
        console.error('Error reading user.json:', err);
        return [];
      }
    }
    public getUserById(id: number): User | undefined {
      const users = this.getAllUsers();
      return users.find((user: User) => user.getId() === id);
    }
    public deleteUser(password: string): void {
      const existingUsers = this.getAllUsers();
      const matchedUser = existingUsers.find(user => user.getPassword() === password);
      if (matchedUser) {
        const filteredUsers = existingUsers.filter(user => user.getId() !== matchedUser.getId());
        this.saveUsersToFile(filteredUsers);
      for (let i = 0; i < filteredUsers.length; i++) {
        filteredUsers[i].setId(i + 1);
        this.saveUsersToFile(filteredUsers);
      }
        console.log("User deleted successfully.");
      } else {
        console.log("Incorrect password. User not found.");
      }
    
    }
  
    //---Admins---
    public addAdmin(admin: Admin): void {
      const existingAdmins = this.getAllAdmins();
      const newId = existingAdmins.length + 1;
      admin.setId(newId);
      existingAdmins.push(admin);
      this.saveAdminsToFile(existingAdmins);
    }
    public getAllAdmins(): Admin[] {
      try {
        const data = fs.readFileSync('Database/admin.json', 'utf-8');
        if (!data) {
          return [];
        }
        const adminData = JSON.parse(data);
        return adminData.map((admin: any) => new Admin(admin.id, admin.firstName, admin.lastName, admin.AdminID));
      } catch (err) {
        console.error('Error reading admin.json:', err);
        return [];
      }
    }
    public getAdminById(id: number): Admin | undefined {
      const admins = this.getAllAdmins();
      return admins.find((admin: Admin) => admin.getId() === id);
    }
    public deleteAdmin(id: number): void {
      const existingAdmins = this.getAllAdmins();
      const filteredAdmins = existingAdmins.filter((admin: Admin) => admin.getId() !== id);
      this.saveAdminsToFile(filteredAdmins);
    }
//---Stations---
  public addStation(station: Station): void {
    const existingStations = this.getAllStations();
    const duplicateStation = existingStations.find(
      existingStation => existingStation.getName().toLowerCase() === station.getName().toLowerCase()
    );
    if (duplicateStation) {
      console.log(`Station with name '${station.getName()}' already exists. Please enter a different name.`)
    } else {
      const newId = existingStations.length + 1;
      station.setId(newId);
      existingStations.push(station);
      this.saveStationsToFile(existingStations);
      console.log(`Station '${station.getName()}' added successfully.`);
    }
  }
  public getAllStations(): Station[] {
    try {
      const data = fs.readFileSync('Database/station.json', 'utf-8');
      if (!data) {
        return [];
      }
      const stationData = JSON.parse(data);
      return stationData.map((station: any) => new Station(station.id, station.name));
    } catch (err) {
      console.error('Error reading station.json:', err);
      return [];
    }
  }
  public getStationById(id: number): Station | undefined {
    const stations = this.getAllStations();
    return stations.find((station: Station) => station.getId() === id);
  }

  public deleteStation(id: number): void {
    const existingStations = this.getAllStations();
    const filteredStations = existingStations.filter((station: Station) => station.getId() != id);
    for (let i = 0; i < filteredStations.length; i++) {
      filteredStations[i].setId(i + 1);
    }
    this.saveStationsToFile(filteredStations);
  }
  


  //---Trains---
  // public addTrain(train: Train): void {
  //   const existingTrains = this.getAllTrains();
  //   const newId = existingTrains.length + 1;
  //   train.setId(newId);
  //   existingTrains.push(train);
  //   this.saveTrainsToFile(existingTrains);
  // }
  public addTrain(train: Train): void {
    const existingTrains = this.getAllTrains();
    const newId = existingTrains.length + 1;
    train.setId(newId);
    const isDuplicate = existingTrains.find(existingTrain => {
      return existingTrain.getStartStation() === train.getStartStation()
        && existingTrain.getEndStation() === train.getEndStation();
    });
    if (isDuplicate) {
      console.log("Поезд с такими же начальной и конечной станциями уже существует");
      return;
    }
    if (train.getStartStation() === train.getEndStation()) {
      console.log("Начальная и конечная станции не могут совпадать");
      return;
    }
    existingTrains.push(train);
    this.saveTrainsToFile(existingTrains);
  }
  
  public getAllTrains(): Train[] {
    try {
      const data = fs.readFileSync('Database/train.json', 'utf-8');
      if (!data) {
        return [];
      }
      const trainData = JSON.parse(data);
      return trainData.map((train: any) => new Train(train.id, train.startStation, train.endStation));
    } catch (err) {
      console.error('Error reading train.json:', err);
      return [];
    }
  }
  public deleteTrain(id: number): void {
    const existingTrains = this.getAllTrains();
    const filteredTrains = existingTrains.filter((train: Train) => train.getId() !== id);
    this.saveTrainsToFile(filteredTrains);
  }
 


  // public showCartTypes(): void {
  //   console.log('--- Select Compartment Cart ---');

  //   const cartTypes = Object.keys(CartSeatsAmount).filter(key => isNaN(Number(key)));

  //   for (let i = 0; i < cartTypes.length; i++) {
  //     console.log(` ${i + 1}. ${cartTypes[i]} Seater (max: ${CartSeatsAmount[cartTypes[i]]})`);
  //   }

  //   let result = readlineSync.question('Enter cart type number ');

  //   const selectedIndex = parseInt(result);

  //   if (isNaN(selectedIndex) || selectedIndex < 1 || selectedIndex > cartTypes.length) {
  //     console.log('Invalid cart type number');
  //     this.showCartTypes();
  //     return;
  //   }

  //   const selectedCartType = cartTypes[selectedIndex - 1];

  //   if (selectedCartType) {
  //     // console.clear();
  //     this..seatReservation();
  //   } else {
  //     console.log('Invalid cart type number');
  //     this.showCartTypes();
  //   }
  // }




  //----Сохранение данных станций в файл----
  private saveStationsToFile(stations: Station[]): void {
    const stationData = stations.map(station => ({
      id: station.getId(),
      name: station.getName()
    }));
    const jsonData = JSON.stringify(stationData, null, 2);
    fs.writeFileSync('Database/station.json', jsonData, 'utf-8');
  }
  private saveTrainsToFile(trains: Train[]): void {
    const trainData = trains.map(train => ({
      id: train.getId(),
      startStation: train.getStartStation(),
      endStation: train.getEndStation()
    }));
    const jsonData = JSON.stringify(trainData, null, 2);
    fs.writeFileSync('Database/train.json', jsonData, 'utf-8');
  }
  //----Сохранение данных пользователей в файл----
  private saveUsersToFile(user: User[]): void {
    const userData = user.map(user => ({
      id: user.getId(),
      firstName: user.getFirstName(),
      lastName: user.getLastName(),
      email: user.getEmail(),
      password: user.getPassword()
    }));
    const jsonData = JSON.stringify(userData, null, 2);
    fs.writeFileSync('Database/user.json', jsonData, 'utf-8');
  }
  //----Сохранение данных администраторов в файл----`
  private saveAdminsToFile(admin: Admin[]): void {
    const adminData = admin.map(admin => ({
      id: admin.getId(),
      firstName: admin.getFirstName(),
      lastName: admin.getLastName(),
      AdminID: admin.getAdminID()
    }));
    const jsonData = JSON.stringify(adminData, null, 2);
    fs.writeFileSync('Database/admin.json', jsonData, 'utf-8');
  }

}
