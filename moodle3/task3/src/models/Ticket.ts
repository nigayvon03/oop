class Ticket {
    id: number = 0;

    constructor(id: number) {
        this.id = id;
    }

    getid(): number {return this.id; }
}
export default Ticket;