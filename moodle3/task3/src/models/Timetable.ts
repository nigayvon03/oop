export class Timetable {
    private id: number;
    private idTrain: number;
    private idDepartureStation: number;
    private idArrivalStation: number;
    private ArrivalTime: Date;
    private DepartureTime: Date;
  
    constructor(
      id: number,
      idTrain: number,
      idDepartureStation: number,
      idArrivalStation: number,
      ArrivalTime: Date,
      DepartureTime: Date
    ) {
      this.id = id;
      this.idTrain = idTrain;
      this.idDepartureStation = idDepartureStation;
      this.idArrivalStation = idArrivalStation;
      this.ArrivalTime = ArrivalTime;
      this.DepartureTime = DepartureTime;
    }
  
    // Add getters and setters for the properties
  }
  