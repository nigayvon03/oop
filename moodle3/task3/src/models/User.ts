import { Roles, Profile } from './Profile';

export class User extends Profile {
  private email: string = '';
  private password: string = '';

  constructor(id: number, firstName: string, lastName: string, email: string, password: string) {
    super(id, firstName, lastName, Roles.USER);
    this.email = email;
    this.password = password;
  }
  
  public getEmail(): string {
    return this.email;
  }

  public setEmail(email: string): void {
    this.email = email;
  }

  public getPassword(): string {
    return this.password;
  }

  public setPassword(password: string): void {
    this.password = password;
  }
  
}
