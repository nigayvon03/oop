import { Roles, Profile } from './Profile';

export class Admin extends Profile {
  private AdminID: number = 0;

  constructor(id: number, firstName: string, lastName: string, AdminID: number) {
    super(id, firstName, lastName, Roles.ADMIN);
    this.AdminID = AdminID;
  }

  public getAdminID(): number {
    return this.AdminID;
  }

  public setAdminID(id: number): void {
    this.AdminID = id;
  }
}
