export enum Roles {
  ADMIN = 'ADMIN',
  USER = 'USER',
}

export abstract class Profile {
  private id: number;
  private firstName: string;
  private lastName: string;
  public role: Roles;

  constructor(id: number, firstName: string, lastName: string, role: Roles) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.role = role;
  }

  public getId(): number {
    return this.id;
  }

  public setId(id: number): void {
    this.id = id;
  }

  public getFirstName(): string {
    return this.firstName;
  }

  public setFirstName(firstName: string): void {
    this.firstName = firstName;
  }

  public getLastName(): string {
    return this.lastName;
  }

  public setLastName(lastName: string): void {
    this.lastName = lastName;
  }
}
