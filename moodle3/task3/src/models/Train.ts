import * as fs from 'fs';
import * as readlineSync from 'readline-sync';
import { Station } from "./Station";

export class Train {
  private id: number;
  private startStation: Station;
  private endStation: Station;

  constructor(id: number, startStation: Station, endStation: Station) {
    this.id = id;
    this.startStation = startStation;
    this.endStation = endStation;
  }
  public getId(): number {
    return this.id;
  }
  public setId(id: number): void {
    this.id = id;
  }
  public getStartStation(): Station {
    return this.startStation;
  }
  public setStartStation(startStation: Station): void {
    this.startStation = startStation;
  }
  public getEndStation(): Station {
    return this.endStation;
  }
  public setEndStation(endStation: Station): void {
    this.endStation = endStation;
  }

 
}
