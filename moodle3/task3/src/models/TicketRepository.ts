export class Seat {
  private readonly id: number;
  private readonly seatId: number;
  private status: boolean;

  constructor(id: number, seatId: number, status: boolean = false) {
    this.id = id;
    this.seatId = seatId;
    this.status = status;
  }

  public getId(): number {
    return this.id;
  }

  public getSeatId(): number {
    return this.seatId;
  }

  public getStatus(): boolean {
    return this.status;
  }

  public setStatus(status: boolean) {
    this.status = status;
  }
}

export enum CartSeatsAmount {
  COMPARTMENT_CART = 4,
  RESERVED_SEAT_CART = 6,
  SLEEPING_CART = 2,
}

export class Cart {
  private readonly id: number;
  private readonly cartId: number;
  private seats: Seat[];

  constructor(id: number, cartId: number, seats: Seat[] = []) {
    this.id = id;
    this.cartId = cartId;
    this.seats = seats;
  }

  public getId(): number {
    return this.id;
  }

  public getCartId(): number {
    return this.cartId;
  }

  public getSeats(): Seat[] {
    return this.seats;
  }
}

export class CompartmentCart extends Cart {
  private readonly seatsAmount: CartSeatsAmount = CartSeatsAmount.COMPARTMENT_CART;

  constructor(id: number, cartId: number, seats: Seat[] = []) {
    if (seats.length === 0) {
      for (let i = 1; i <= CartSeatsAmount.COMPARTMENT_CART; i++) {
        seats.push(new Seat(1, i));
      }
    }
    super(id, cartId, seats);
  }

  public getSeatsAmount(): number {
    return this.seatsAmount;
  }
}

export class ReservedSeatCart extends Cart {
  private readonly seatsAmount: CartSeatsAmount = CartSeatsAmount.RESERVED_SEAT_CART;

  constructor(id: number, cartId: number, seats: Seat[] = []) {
    if (seats.length === 0) {
      for (let i = 1; i <= CartSeatsAmount.RESERVED_SEAT_CART; i++) {
        seats.push(new Seat(2, i));
      }
    }
    super(id, cartId, seats);
  }

  public getSeatsAmount(): number {
    return this.seatsAmount;
  }
}

export class SleepingCart extends Cart {
  private readonly seatsAmount: CartSeatsAmount = CartSeatsAmount.SLEEPING_CART;

  constructor(id: number, cartId: number, seats: Seat[] = []) {
    if (seats.length === 0) {
      for (let i = 1; i <= CartSeatsAmount.SLEEPING_CART; i++) {
        seats.push(new Seat(3, i));
      }
    }
    super(id, cartId, seats);
  }

  public getSeatsAmount(): number {
    return this.seatsAmount;
  }
}
