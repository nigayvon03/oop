import { Database } from './Database';
import { Station } from './models/Station';
import { User } from './models/User';
import { Admin } from './models/Admin';
import { Train } from './models/Train';

class Backend {
    private db: Database;
    constructor() {
        this.db = new Database();
    }
    // ----------------------------------------------------------------
    // CRUD methods
    // ----------------------------------------------------------------
    //---Users---
    public addUser (firstName: string, lastName: string, email: string, password: string) :void {
        this.db.addUser(new User(1, firstName, lastName, email, password));
    }
    public deleteUser (password: string) :void {
        this.db.deleteUser(password);
    }
    //---Admins---
    public addAdmin (firstName: string, lastName: string, AdminID: number) :void {
        this.db.addAdmin(new Admin(1, firstName, lastName, AdminID));
    }
    public deleteAdmin (id: number) :void {
        this.db.deleteAdmin(id);
    }
    //---Stations---
    public addStation (stationName: string) :void {
        this.db.addStation(new Station(1,stationName));
    }
    public getAllStation () : Station[] {
        return this.db.getAllStations()
    }
    public deleteStation(id: number) :void {
        this.db.deleteStation(id);
    }
    //---Trains---
    public addTrain (startStationName: string, endStationName: string) :void {
        this.db.addTrain(new Train(1, new Station(1, startStationName), new Station(1, endStationName)));
    }
    public deleteTrain (id: number) :void {
        this.db.deleteTrain(id);
    }
    

    
    // ----------------------------------------------------------------
    // Busines  
    // проверка на существование пользователя
    public checkUser(firstName: string, lastName: string) : boolean {
        const allUsers = this.db.getAllUsers();
        const foundUser = allUsers.filter(
            user => user.getFirstName() == firstName && 
            user.getLastName() == lastName
        );
        return !!foundUser;
    }
      
    public checkAdmin(firstName: string, lastName: string, AdminID: number): boolean {
        const allAdmins = this.db.getAllAdmins();
        const foundAdmin = allAdmins.find(admin => admin.getFirstName() === firstName && admin.getLastName() === lastName && admin.getAdminID() === AdminID);
        return !!foundAdmin;
      }
      private isStationExist(stationName: string): boolean {
        const stationsData = this.db.getAllStations();
        const foundStation = stationsData.find(station => station.getName() === stationName);
        return !!foundStation;
      }
   
}

export default Backend;