import * as readlineSync from 'readline-sync';
import { Frontend } from './Frontend';

export class App {
  private frontend: Frontend;

  constructor() {
    this.frontend = new Frontend();
  }

  public start(): void {
    while (true) {
      let result = this.frontend.showMenu();
      if (result === 42)
        break;
    }
  }
}

