//  2
// Типы данных
var number = 5; // Числовой тип данных
var string = "Hello World!"; // Строковый тип данных
var boolean = false; // Логический тип данных
var object = {}; // Тип данных "объект"
var array = []; // Тип данных "Массив"

// Вывод типов данных 
console.log(typeof number); // В консоле выведен тип данных "number"
console.log(typeof string); // В консоле выведен тип данных "string"
console.log(typeof boolean); // В консоле выведен тип данных "boolean"
console.log(typeof object); // В консоле выведен тип данных "object"
console.log(typeof array); // В консоле выведен тип данных "object"

// Максимальные и минимальные значения числовых типов данных
console.log(Number.MAX_VALUE); // Максимальное значение числового типа данных
console.log(Number.MIN_VALUE); // Минимальное положительное значение числового типа данных
